<?php

namespace Tests;

use App\Services\ContactService;
use PHPUnit\Framework\TestCase;

class ContactServiceTest extends TestCase
{
    /** @test */
    public function returns_null_when_contact_not_found()
    {
        $this->assertNull(ContactService::findByName('contact4'));
    }

}
