<?php

namespace Tests;

use App\Services\ContactService;
use PHPUnit\Framework\TestCase;
use \Mockery;

class CarrierTest extends TestCase
{
    /** @test */
    public function sendSms_returns_false_when_number_has_not_numeric_values()
    {
        $provider = Mockery::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('sendSms')->once()->andReturnUsing(function ($number, $body) {
            if (ContactService::validateNumber($number)) {
                // send sms
                return true;
            }
            return false;
        });

        $this->assertFalse($provider->sendSms('abdcefgh', 'body'));
    }

    /** @test */
    public function sendSms_returns_false_when_number_length_not_equals_to_nine()
    {
        $provider = Mockery::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('sendSms')->once()->andReturnUsing(function ($number, $body) {
            if (ContactService::validateNumber($number)) {
                // send sms
                return true;
            }
            return false;
        });

        $this->assertFalse($provider->sendSms('12345678', 'body'));
    }

    /** @test */
    public function sendSms_returns_true_when_number_is_valid()
    {
        $provider = Mockery::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('sendSms')->once()->andReturnUsing(function ($number, $body) {
            if (ContactService::validateNumber($number)) {
                // send sms
                return true;
            }
            return false;
        });

        $this->assertTrue($provider->sendSms('123456789', 'body'));
    }

}
