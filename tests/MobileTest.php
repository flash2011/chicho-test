<?php

namespace Tests;

use App\Call;
use App\Contact;
use App\Mobile;
use PHPUnit\Framework\TestCase;
use \Mockery;

class MobileTest extends TestCase
{
    /** @test */
    public function it_returns_null_when_name_empty()
    {
        $provider = Mockery::mock('App\Interfaces\CarrierInterface');
        $mobile = new Mobile($provider);

        $this->assertNull($mobile->makeCallByName(''));
    }

    /** @test */
    public function it_returns_null_when_name_empty_with_Carrier1()
    {
        $provider = Mockery::mock('App\Carrier1');
        $mobile = new Mobile($provider);

        $this->assertNull($mobile->makeCallByName(''));
    }

    /** @test */
    public function it_returns_call_when_name_valid()
    {
        $provider = Mockery::mock('App\Interfaces\CarrierInterface');
        $provider->shouldReceive('dialContact')->once()->with(Contact::class);
        $provider->shouldReceive('makeCall')->once()->andReturn(new Call);

        $contactService = Mockery::mock('App\Services\ContactService');
        $contactService->shouldReceive('findByName')->once()->with('Test')->andReturn(new Contact('Test'));

        $mobile = new Mobile($provider, $contactService);

        $this->assertInstanceOf('App\Call', $mobile->makeCallByName('Test'));
    }

    /** @test */
    public function it_returns_call_when_name_valid_with_carrier2()
    {
        $provider = Mockery::mock('App\Carrier2');
        $provider->shouldReceive('dialContact')->once()->with(Contact::class);
        $provider->shouldReceive('makeCall')->once()->andReturn(new Call);

        $contactService = Mockery::mock('App\Services\ContactService');
        $contactService->shouldReceive('findByName')->once()->with('Test')->andReturn(new Contact('Test'));

        $mobile = new Mobile($provider, $contactService);

        $this->assertInstanceOf('App\Call', $mobile->makeCallByName('Test'));
    }

}
