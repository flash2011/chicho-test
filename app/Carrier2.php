<?php

namespace App;

use App\Interfaces\CarrierInterface;

class Carrier2 implements CarrierInterface
{

    public function dialContact(Contact $contact)
    {
        //logic
    }

    public function makeCall(): Call
    {
        //logic
    }

    public function sendSms(string $number, string $body): bool
    {
        //logic
    }
}
