<?php

namespace App\Services;

use App\Contact;

class ContactService
{

    public static function findByName($name): ?Contact
    {
        // queries to the db
        require_once 'App\Api\contact.php';
        foreach ($records as $record) {
            if ($record->getName() == $name) {
                return $record;
            }

        }
        return null;
    }

    public static function validateNumber(string $number): bool
    {
        // logic to validate numbers
        if (\is_numeric($number) && \strlen($number) == 9) {
            return true;
        }

        return false;
    }
}
