<?php

namespace App;

class Contact
{
    protected $name;
    public function __construct($name)
    {
        # code...
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
