<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

class Mobile
{

    protected $provider;
    protected $contactService;

    public function __construct(CarrierInterface $provider, ContactService $contactService = null)
    {
        $this->provider = $provider;
        $this->contactService = $contactService;
    }

    public function makeCallByName($name = '')
    {
        if (empty($name)) {
            return;
        }

        if ($this->contactService == null) {
            $contact = ContactService::findByName($name);
        } else {
            $contact = $this->contactService->findByName($name);
        }

        $this->provider->dialContact($contact);

        return $this->provider->makeCall();
    }

}
